#include <cstring>
#include "openssl-bio-fetch.h"
#include <emojicode/s/String.h>
#include <emojicode/runtime/Runtime.h>
#include <vector>
#include <numeric>

int verify_callback(int preverify, X509_STORE_CTX* x509_ctx);

void init_openssl_library(void);
void print_cn_name(const char* label, X509_NAME* const name);
void print_san_name(const char* label, X509* const cert);
void print_error_string(unsigned long err, const char* const label);
extern "C" s::String* dewit(s::String* name, s::String* name_port, s::String* request);

const char* PREFERRED_CIPHERS =

/* TLS 1.2 only */
"ECDHE-ECDSA-AES256-GCM-SHA384:"
"ECDHE-RSA-AES256-GCM-SHA384:"
"ECDHE-ECDSA-AES128-GCM-SHA256:"
"ECDHE-RSA-AES128-GCM-SHA256:"

/* TLS 1.2 only */
"DHE-DSS-AES256-GCM-SHA384:"
"DHE-RSA-AES256-GCM-SHA384:"
"DHE-DSS-AES128-GCM-SHA256:"
"DHE-RSA-AES128-GCM-SHA256:"

/* TLS 1.0 only */
"DHE-DSS-AES256-SHA:"
"DHE-RSA-AES256-SHA:"
"DHE-DSS-AES128-SHA:"
"DHE-RSA-AES128-SHA:"

/* SSL 3.0 and TLS 1.0 */
"EDH-DSS-DES-CBC3-SHA:"
"EDH-RSA-DES-CBC3-SHA:"
"DH-DSS-DES-CBC3-SHA:"
"DH-RSA-DES-CBC3-SHA";

extern "C" s::String* dewit(s::String* name, s::String* name_port, s::String* request) {

#if !defined(NDEBUG)
    InstallDebugTrapHandler();
#endif

    char host_name[name->count+3] = {};
    memcpy(host_name, name->characters.get(), name->count);
    char host_name_port[name_port->count+3] = {};
    memcpy(host_name_port, name_port->characters.get(), name_port->count);

    long res = 1;
    auto ret = s::String::init("");
    unsigned long ssl_err = 0;

    SSL_CTX* ctx = NULL;
    BIO *web = NULL, *out = NULL;
    SSL *ssl = NULL;

    /* Internal function that wraps the OpenSSL init's   */
    /* Cannot fail because no OpenSSL function fails ??? */
    init_openssl_library();

    /* https://www.openssl.org/docs/ssl/SSL_CTX_new.html */
    const SSL_METHOD* method = SSLv23_method();
    ssl_err = ERR_get_error();
    ASSERT(NULL != method);
    if(!(NULL != method))
    {
        print_error_string(ssl_err, "SSLv23_method");
        return ret;
    }

    /* http://www.openssl.org/docs/ssl/ctx_new.html */
    ctx = SSL_CTX_new(method);
    /* ctx = SSL_CTX_new(TLSv1_method()); */
    ssl_err = ERR_get_error();

    ASSERT(ctx != NULL);
    if(!(ctx != NULL))
    {
        print_error_string(ssl_err, "SSL_CTX_new");
        return ret;
    }

    /* https://www.openssl.org/docs/ssl/ctx_set_verify.html */
    SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER, verify_callback);
    /* Cannot fail ??? */

    /* https://www.openssl.org/docs/ssl/ctx_set_verify.html */
    SSL_CTX_set_verify_depth(ctx, 5);
    /* Cannot fail ??? */

    /* Remove the most egregious. Because SSLv2 and SSLv3 have been      */
    /* removed, a TLSv1.0 handshake is used. The client accepts TLSv1.0  */
    /* and above. An added benefit of TLS 1.0 and above are TLS          */
    /* extensions like Server Name Indicatior (SNI).                     */
    const long flags = SSL_OP_ALL | SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3 | SSL_OP_NO_COMPRESSION;
    long old_opts = SSL_CTX_set_options(ctx, flags);
    UNUSED(old_opts);

    /* http://www.openssl.org/docs/ssl/SSL_CTX_load_verify_locations.html */
    res = SSL_CTX_load_verify_locations(ctx, "discord.pem", NULL);
    ssl_err = ERR_get_error();

    ASSERT(1 == res);
    if(!(1 == res))
    {
        /* Non-fatal, but something else will probably break later */
        print_error_string(ssl_err, "SSL_CTX_load_verify_locations");
        /* break; */
    }

    /* https://www.openssl.org/docs/crypto/BIO_f_ssl.html */
    web = BIO_new_ssl_connect(ctx);
    ssl_err = ERR_get_error();

    ASSERT(web != NULL);
    if(!(web != NULL))
    {
        print_error_string(ssl_err, "BIO_new_ssl_connect");
        return ret;
    }

    /* https://www.openssl.org/docs/crypto/BIO_s_connect.html */
    res = BIO_set_conn_hostname(web, host_name_port);
    ssl_err = ERR_get_error();

    ASSERT(1 == res);
    if(!(1 == res))
    {
        print_error_string(ssl_err, "BIO_set_conn_hostname");
        return ret;
    }

    /* https://www.openssl.org/docs/crypto/BIO_f_ssl.html */
    /* This copies an internal pointer. No need to free.  */
    BIO_get_ssl(web, &ssl);
    ssl_err = ERR_get_error();

    ASSERT(ssl != NULL);
    if(!(ssl != NULL))
    {
        print_error_string(ssl_err, "BIO_get_ssl");
        return ret;
    }

    /* https://www.openssl.org/docs/ssl/ssl.html#DEALING_WITH_PROTOCOL_CONTEXTS */
    /* https://www.openssl.org/docs/ssl/SSL_CTX_set_cipher_list.html            */
    res = SSL_set_cipher_list(ssl, PREFERRED_CIPHERS);
    ssl_err = ERR_get_error();

    ASSERT(1 == res);
    if(!(1 == res))
    {
        print_error_string(ssl_err, "SSL_set_cipher_list");
        return ret;
    }

    /* No documentation. See the source code for tls.h and s_client.c */
    res = SSL_set_tlsext_host_name(ssl, host_name);
    ssl_err = ERR_get_error();

    ASSERT(1 == res);
    if(!(1 == res))
    {
        /* Non-fatal, but who knows what cert might be served by an SNI server  */
        /* (We know its the default site's cert in Apache and IIS...)           */
        print_error_string(ssl_err, "SSL_set_tlsext_host_name");
        /* break; */
    }

    /* https://www.openssl.org/docs/crypto/BIO_s_file.html */
    out = BIO_new_fp(fopen("/tmp/edbot", "w"), BIO_NOCLOSE);
    ssl_err = ERR_get_error();

    ASSERT(NULL != out);
    if(!(NULL != out))
    {
        print_error_string(ssl_err, "BIO_new_fp");
        return ret;
    }

    /* https://www.openssl.org/docs/crypto/BIO_s_connect.html */
    res = BIO_do_connect(web);
    ssl_err = ERR_get_error();

    ASSERT(1 == res);
    if(!(1 == res))
    {
        print_error_string(ssl_err, "BIO_do_connect");
        return ret;
    }

    /* https://www.openssl.org/docs/crypto/BIO_f_ssl.html */
    res = BIO_do_handshake(web);
    ssl_err = ERR_get_error();

    ASSERT(1 == res);
    if(!(1 == res))
    {
        print_error_string(ssl_err, "BIO_do_handshake");
        return ret;
    }

    /* Step 1: verify a server certifcate was presented during negotiation */
    /* https://www.openssl.org/docs/ssl/SSL_get_peer_certificate.html          */
    X509* cert = SSL_get_peer_certificate(ssl);
    if(cert) { X509_free(cert); } /* Free immediately */

    ASSERT(NULL != cert);
    if(NULL == cert)
    {
        /* Hack a code for print_error_string. */
        print_error_string(X509_V_ERR_APPLICATION_VERIFICATION, "SSL_get_peer_certificate");
        return ret;
    }

    /* Step 2: verify the result of chain verifcation             */
    /* http://www.openssl.org/docs/ssl/SSL_get_verify_result.html */
    /* Error codes: http://www.openssl.org/docs/apps/verify.html  */
    res = SSL_get_verify_result(ssl);

    //always fails for some reason
    //ASSERT(X509_V_OK == res);
    //if(!(X509_V_OK == res))
    //{
    //    /* Hack a code into print_error_string. */
    //    print_error_string((unsigned long)res, "SSL_get_verify_results");
    //    break; /* failed */
    //}

    /* Now, we can finally start reading and writing to the BIO...*/

    BIO_puts(web, request->characters.get());

    int len = 0;
    char buff[1536] = {};

    /* https://www.openssl.org/docs/crypto/BIO_read.html */
    len = BIO_read(web, buff, sizeof(buff));
    //for(int i = 0; i < 1536; i++)
        //printf("%d %d %c - \n", i, buff[i], buff[i]);

    if(len > 0) {
        ret->store(buff);
    }
        //BIO_write(out, buff, len);

    /* BIO_should_retry returns TRUE unless there's an  */
    /* error. We expect an error when the server        */
    /* provides the response and closes the connection. */

    if(out)
        BIO_free(out);

    if(web != NULL)
        BIO_free_all(web);

    if(NULL != ctx)
        SSL_CTX_free(ctx);
    return ret;
}

void init_openssl_library(void)
{
    /* https://www.openssl.org/docs/ssl/SSL_library_init.html */
    (void)SSL_library_init();
    /* Cannot fail (always returns success) ??? */

    /* https://www.openssl.org/docs/crypto/ERR_load_crypto_strings.html */
    SSL_load_error_strings();
    /* Cannot fail ??? */

    /* SSL_load_error_strings loads both libssl and libcrypto strings */
    /* ERR_load_crypto_strings(); */
    /* Cannot fail ??? */

    /* OpenSSL_config may or may not be called internally, based on */
    /*  some #defines and internal gyrations. Explicitly call it    */
    /*  *IF* you need something from openssl.cfg, such as a         */
    /*  dynamically configured ENGINE.                              */
    OPENSSL_config(NULL);
    /* Cannot fail ??? */
}

void print_cn_name(const char* label, X509_NAME* const name)
{
    int idx = -1, success = 0;
    unsigned char *utf8 = NULL;

    do
    {
        if(!name) break; /* failed */

        idx = X509_NAME_get_index_by_NID(name, NID_commonName, -1);
        if(!(idx > -1))  break; /* failed */

        X509_NAME_ENTRY* entry = X509_NAME_get_entry(name, idx);
        if(!entry) break; /* failed */

        ASN1_STRING* data = X509_NAME_ENTRY_get_data(entry);
        if(!data) break; /* failed */

        int length = ASN1_STRING_to_UTF8(&utf8, data);
        if(!utf8 || !(length > 0))  break; /* failed */

        fprintf(stdout, "  %s: %s\n", label, utf8);
        success = 1;

    } while (0);

    if(utf8)
        OPENSSL_free(utf8);

    if(!success)
        fprintf(stdout, "  %s: <not available>\n", label);
}

void print_san_name(const char* label, X509* const cert)
{
    int success = 0;
    GENERAL_NAMES* names = NULL;
    unsigned char* utf8 = NULL;

    do
    {
        if(!cert) break; /* failed */

        names = (GENERAL_NAMES*)X509_get_ext_d2i(cert, NID_subject_alt_name, 0, 0 );
        if(!names) break;

        int i = 0, count = sk_GENERAL_NAME_num(names);
        if(!count) break; /* failed */

        for( i = 0; i < count; ++i )
        {
            GENERAL_NAME* entry = sk_GENERAL_NAME_value(names, i);
            if(!entry) continue;

            if(GEN_DNS == entry->type)
            {
                int len1 = 0, len2 = -1;

                len1 = ASN1_STRING_to_UTF8(&utf8, entry->d.dNSName);
                if(utf8) {
                    len2 = (int)strlen((const char*)utf8);
                }

                if(len1 != len2) {
                    fprintf(stderr, "  Strlen and ASN1_STRING size do not match (embedded null?): %d vs %d\n", len2, len1);
                }

                /* If there's a problem with string lengths, then     */
                /* we skip the candidate and move on to the next.     */
                /* Another policy would be to fails since it probably */
                /* indicates the client is under attack.              */
                if(utf8 && len1 && len2 && (len1 == len2)) {
                    fprintf(stdout, "  %s: %s\n", label, utf8);
                    success = 1;
                }

                if(utf8) {
                    OPENSSL_free(utf8), utf8 = NULL;
                }
            }
            else
            {
                fprintf(stderr, "  Unknown GENERAL_NAME type: %d\n", entry->type);
            }
        }

    } while (0);

    if(names)
        GENERAL_NAMES_free(names);

    if(utf8)
        OPENSSL_free(utf8);

    if(!success)
        fprintf(stdout, "  %s: <not available>\n", label);

}

int verify_callback(int preverify, X509_STORE_CTX* x509_ctx)
{
    /* For error codes, see http://www.openssl.org/docs/apps/verify.html  */

    int depth = X509_STORE_CTX_get_error_depth(x509_ctx);
    int err = X509_STORE_CTX_get_error(x509_ctx);

    X509* cert = X509_STORE_CTX_get_current_cert(x509_ctx);
    X509_NAME* iname = cert ? X509_get_issuer_name(cert) : NULL;
    X509_NAME* sname = cert ? X509_get_subject_name(cert) : NULL;

    //eh
    //fprintf(stdout, "verify_callback (depth=%d)(preverify=%d)\n", depth, preverify);


    //eh
    /* Issuer is the authority we trust that warrants nothing useful */
    //print_cn_name("Issuer (cn)", iname);

    /* Subject is who the certificate is issued to by the authority  */
    //print_cn_name("Subject (cn)", sname);

    if(depth == 0) {
        /* If depth is 0, its the server's certificate. Print the SANs */
        //print_san_name("Subject (san)", cert);
    }


    //eh
    /*if(preverify == 0)
    {
        if(err == X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT_LOCALLY)
            fprintf(stdout, "  Error = X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT_LOCALLY\n");
        else if(err == X509_V_ERR_CERT_UNTRUSTED)
            fprintf(stdout, "  Error = X509_V_ERR_CERT_UNTRUSTED\n");
        else if(err == X509_V_ERR_SELF_SIGNED_CERT_IN_CHAIN)
            fprintf(stdout, "  Error = X509_V_ERR_SELF_SIGNED_CERT_IN_CHAIN\n");
        else if(err == X509_V_ERR_CERT_NOT_YET_VALID)
            fprintf(stdout, "  Error = X509_V_ERR_CERT_NOT_YET_VALID\n");
        else if(err == X509_V_ERR_CERT_HAS_EXPIRED)
            fprintf(stdout, "  Error = X509_V_ERR_CERT_HAS_EXPIRED\n");
        else if(err == X509_V_OK)
            fprintf(stdout, "  Error = X509_V_OK\n");
        else
            fprintf(stdout, "  Error = %d\n", err);
    }*/


#if !defined(NDEBUG)
    return 1;
#else
    return preverify;
#endif
}

void print_error_string(unsigned long err, const char* const label)
{
    const char* const str = ERR_reason_error_string(err);
    if(str)
        fprintf(stderr, "%s\n", str);
    else
        fprintf(stderr, "%s failed: %lu (0x%lx)\n", label, err, err);
}
